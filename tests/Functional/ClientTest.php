<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientTest extends WebTestCase
{
    public function testShouldDisplayClientIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client index');
    }

    public function testShouldDisplayCreateNewClient()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Client');
    }

    public function testShouldAddNewClient()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        // Simuler l'ajout d'un nouveau client avec des données uniques
        $cin = uniqid();
        $nom = 'Hamda';
        $prenom = 'Souiden';
        $adresse = 'Tunis';

        $form = $buttonCrawlerNode->form([
            'client[cin]' => $cin,
            'client[nom]' => $nom,
            'client[prenom]' => $prenom,
            'client[adresse]' => $adresse,
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $cin);
        $this->assertSelectorTextContains('body', $nom);
        $this->assertSelectorTextContains('body', $prenom);
        $this->assertSelectorTextContains('body', $adresse);
    }
}
