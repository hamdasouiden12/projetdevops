<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationTest extends WebTestCase
{
    public function testShouldDisplayLocationIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location index');
    }

    /*public function testShouldDisplayCreateNewLocation()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/client/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        // Simuler l'ajout d'un nouveau client avec des données uniques
        $cin = uniqid();
        $nom = 'Hamda';
        $prenom = 'Souiden';
        $adresse = 'Tunis';

        $form = $buttonCrawlerNode->form([
            'client[cin]' => $cin,
            'client[nom]' => $nom,
            'client[prenom]' => $prenom,
            'client[adresse]' => $adresse,
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $cin);
        $this->assertSelectorTextContains('body', $nom);
        $this->assertSelectorTextContains('body', $prenom);
        $this->assertSelectorTextContains('body', $adresse);

        $crawler = $client->request('GET', '/voiture/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        // Simuler l'ajout d'une nouvelle voiture avec des données uniques
        $serie = uniqid();
        $modele = 'Tesla';
        $prixJour = 50;

        $form = $buttonCrawlerNode->form([
            'voiture[serie]' => $serie,
            'voiture[modele]' => $modele,
            'voiture[prixJour]' => $prixJour,
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $serie);
        $this->assertSelectorTextContains('body', $modele);
        $this->assertSelectorTextContains('body', $prixJour);
        $client->followRedirects();
        $crawler = $client->request('GET', '/location/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Location');
    }*/

    /*public function testShouldAddNewLocation()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        // Simuler l'ajout d'une nouvelle location avec des données uniques
        $prixTotal = 200;

        $form = $buttonCrawlerNode->form([
            'location[prixTotal]' => $prixTotal,
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $prixTotal);
    }*/
}
