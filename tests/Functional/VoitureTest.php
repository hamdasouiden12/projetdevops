<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureTest extends WebTestCase
{
     public function testShouldDisplayVoitureIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Voiture index');
    }

    public function testShouldDisplayCreateNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Voiture');
    }

    public function testShouldAddNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        // Simuler l'ajout d'une nouvelle voiture avec des données uniques
        $serie = uniqid();
        $modele = 'Tesla';
        $prixJour = 50;

        $form = $buttonCrawlerNode->form([
            'voiture[serie]' => $serie,
            'voiture[modele]' => $modele,
            'voiture[prixJour]' => $prixJour,
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $serie);
        $this->assertSelectorTextContains('body', $modele);
        $this->assertSelectorTextContains('body', $prixJour);
    }
}
