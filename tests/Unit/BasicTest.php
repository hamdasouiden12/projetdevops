<?php

namespace App\Tests\Unit;

use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{
    public function testVoiture(): void
    {
        $voiture = new Voiture();

        // Définit les valeurs des attributs
        $voiture->setSerie('ABC123');
        $voiture->setModele('Tesla');
        $voiture->setPrixJour(50);

        // Vérifie si les valeurs sont correctement définies
        $this->assertTrue($voiture->getSerie() === 'ABC123');
        $this->assertTrue($voiture->getModele() === 'Tesla');
        $this->assertTrue($voiture->getPrixJour() === 50);
    }
}
