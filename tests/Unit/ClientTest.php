<?php

namespace App\Tests\Unit;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClient(): void
    {
        $client = new Client();

        // Définit les valeurs des attributs
        $client->setCin('1237823');
        $client->setNom('Hamda');
        $client->setPrenom('Souiden');
        $client->setAdresse('Tunis');

        // Vérifie si les valeurs sont correctement définies
        $this->assertTrue($client->getCin() === '1237823');
        $this->assertTrue($client->getNom() === 'Hamda');
        $this->assertTrue($client->getPrenom() === 'Souiden');
        $this->assertTrue($client->getAdresse() === 'Tunis');
    }
}
