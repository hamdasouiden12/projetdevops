<?php

namespace App\Tests\Unit;

use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testLocation(): void
    {
         $location = new Location();

        // Définit les valeurs des attributs
        $location->setPrixTotal(150.0);

        // Vérifie si les valeurs sont correctement définies
        $this->assertTrue($location->getPrixTotal() === 150.0);
    }
}
